@set DBName=finstatdevel
@set MYSQLHOST="localhost"
@set MYSQLDIR=C:\Program Files\MySQL\MySQL Server 5.7\bin\
@set DUMP_OPTIONS=--hex-blob --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert

@echo %time% recreate database %DBName%
@set SQL_DROP_CREATE=drop database %DBName%; create database %DBName% default character set utf8;
@echo %SQL_DROP_CREATE% | %~dp0\..\db\run_mysql.bat
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo %time% prepare empty tables for %DBName%
@call %~dp0\..\db\run_mysql.bat < %~dp0\..\db\backups\empty.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@call :load-data 2016 data-20191128-structure-20161231.txt
@call :load-data 2017 data-20191029-structure-20171231.txt
@call :load-data 2018 data-20191029-structure-20181231.txt

exit

@rem -----------------------------------------------------------
:load-data
@set DATA_YEAR=%1
@set DATA_FILE=%2
@echo %time% load %DATA_YEAR%\%DATA_FILE% {
@pushd %~dp0%DATA_YEAR%
@call %~dp0\..\db\run_mysql.bat < load.etalon.sql
@popd 
@echo %time%   ok %DATA_YEAR%\%DATA_FILE% }
@exit /B
