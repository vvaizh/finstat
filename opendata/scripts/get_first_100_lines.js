
if (WScript.FullName.indexOf("cscript.exe")==-1)
{
  WScript.Echo("This script may be runned only with CScript..  Try to run create_sql.bat")
  WScript.Quit(1)
}

var fso = new ActiveXObject("Scripting.FileSystemObject");
var shell= new ActiveXObject("WScript.Shell");

var file_name_src= WScript.arguments.Item(0);
var file_name_dst= WScript.arguments.Item(1);

WScript.Echo('src filename: ' + file_name_src);
var f= fso.OpenTextFile(file_name_src, 1);
WScript.Echo('dst filename: ' + file_name_dst);
var new_f= fso.CreateTextFile(file_name_dst, true);
var count_lines= 0;
while (!f.AtEndOfStream && count_lines<100)
{
    var line= f.ReadLine();
    new_f.WriteLine(line);
    count_lines++;
}
f.Close();
new_f.Close();
