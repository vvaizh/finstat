/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Prepare_table.sql.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */




LOCK TABLES finstat WRITE, finstat_subject WRITE, finstat_year_portion WRITE;

insert into finstat_year_portion
set year= 2016, FileName= "data.small.2016.txt";
set @id_finstat_year_portion= last_insert_id();

DROP TEMPORARY TABLE IF EXISTS finstat_csv;
CREATE TEMPORARY TABLE finstat_csv
(
	inn                   VARCHAR(10)    NOT NULL,
	name                  TEXT           NOT NULL,
	okpo                  VARCHAR(8)     NOT NULL,
	okopf                 VARCHAR(5)     NOT NULL,
	okfs                  VARCHAR(2)     NOT NULL,
	okved                 VARCHAR(9)     NOT NULL,

	row_number           INT
	

 , f1110 BIGINT NOT NULL
 , f1120 BIGINT NOT NULL
 , f1130 BIGINT NOT NULL
 , f1140 BIGINT NOT NULL
 , f1150 BIGINT NOT NULL
 , f1160 BIGINT NOT NULL
 , f1170 BIGINT NOT NULL
 , f1180 BIGINT NOT NULL
 , f1190 BIGINT NOT NULL
 , f1100 BIGINT NOT NULL
 , f1210 BIGINT NOT NULL
 , f1220 BIGINT NOT NULL
 , f1230 BIGINT NOT NULL
 , f1240 BIGINT NOT NULL
 , f1250 BIGINT NOT NULL
 , f1260 BIGINT NOT NULL
 , f1200 BIGINT NOT NULL
 , f1600 BIGINT NOT NULL
 , f1310 BIGINT NOT NULL
 , f1320 BIGINT NOT NULL
 , f1340 BIGINT NOT NULL
 , f1350 BIGINT NOT NULL
 , f1360 BIGINT NOT NULL
 , f1370 BIGINT NOT NULL
 , f1300 BIGINT NOT NULL
 , f1410 BIGINT NOT NULL
 , f1420 BIGINT NOT NULL
 , f1430 BIGINT NOT NULL
 , f1450 BIGINT NOT NULL
 , f1400 BIGINT NOT NULL
 , f1510 BIGINT NOT NULL
 , f1520 BIGINT NOT NULL
 , f1530 BIGINT NOT NULL
 , f1540 BIGINT NOT NULL
 , f1550 BIGINT NOT NULL
 , f1500 BIGINT NOT NULL
 , f1700 BIGINT NOT NULL
 , f2110 BIGINT NOT NULL
 , f2120 BIGINT NOT NULL
 , f2100 BIGINT NOT NULL
 , f2210 BIGINT NOT NULL
 , f2220 BIGINT NOT NULL
 , f2200 BIGINT NOT NULL
 , f2310 BIGINT NOT NULL
 , f2320 BIGINT NOT NULL
 , f2330 BIGINT NOT NULL
 , f2340 BIGINT NOT NULL
 , f2350 BIGINT NOT NULL
 , f2300 BIGINT NOT NULL
 , f2410 BIGINT NOT NULL
 , f2421 BIGINT NOT NULL
 , f2430 BIGINT NOT NULL
 , f2450 BIGINT NOT NULL
 , f2460 BIGINT NOT NULL
 , f2400 BIGINT NOT NULL
 , f2510 BIGINT NOT NULL
 , f2520 BIGINT NOT NULL
 , f2500 BIGINT NOT NULL

)
;

set @row_number = 0;
select 2016 year, now() 'start load data.small.2016.txt';

LOAD DATA LOCAL INFILE 
  'data.small.2016.txt'
INTO TABLE finstat_csv 

CHARACTER SET cp1251
FIELDS TERMINATED BY ';'
ESCAPED BY '$'
LINES TERMINATED BY '\r\n'
(

   @name
 , @okpo
 , @okopf
 , @okfs
 , @okved
 , @inn
 , @measure
 , @type

 , @f11103
 , @f11104
 , @f11203
 , @f11204
 , @f11303
 , @f11304
 , @f11403
 , @f11404
 , @f11503
 , @f11504
 , @f11603
 , @f11604
 , @f11703
 , @f11704
 , @f11803
 , @f11804
 , @f11903
 , @f11904
 , @f11003
 , @f11004
 , @f12103
 , @f12104
 , @f12203
 , @f12204
 , @f12303
 , @f12304
 , @f12403
 , @f12404
 , @f12503
 , @f12504
 , @f12603
 , @f12604
 , @f12003
 , @f12004
 , @f16003
 , @f16004
 , @f13103
 , @f13104
 , @f13203
 , @f13204
 , @f13403
 , @f13404
 , @f13503
 , @f13504
 , @f13603
 , @f13604
 , @f13703
 , @f13704
 , @f13003
 , @f13004
 , @f14103
 , @f14104
 , @f14203
 , @f14204
 , @f14303
 , @f14304
 , @f14503
 , @f14504
 , @f14003
 , @f14004
 , @f15103
 , @f15104
 , @f15203
 , @f15204
 , @f15303
 , @f15304
 , @f15403
 , @f15404
 , @f15503
 , @f15504
 , @f15003
 , @f15004
 , @f17003
 , @f17004
 , @f21103
 , @f21104
 , @f21203
 , @f21204
 , @f21003
 , @f21004
 , @f22103
 , @f22104
 , @f22203
 , @f22204
 , @f22003
 , @f22004
 , @f23103
 , @f23104
 , @f23203
 , @f23204
 , @f23303
 , @f23304
 , @f23403
 , @f23404
 , @f23503
 , @f23504
 , @f23003
 , @f23004
 , @f24103
 , @f24104
 , @f24213
 , @f24214
 , @f24303
 , @f24304
 , @f24503
 , @f24504
 , @f24603
 , @f24604
 , @f24003
 , @f24004
 , @f25103
 , @f25104
 , @f25203
 , @f25204
 , @f25003
 , @f25004
 , @f32003
 , @f32004
 , @f32005
 , @f32006
 , @f32007
 , @f32008
 , @f33103
 , @f33104
 , @f33105
 , @f33106
 , @f33107
 , @f33108
 , @f33117
 , @f33118
 , @f33125
 , @f33127
 , @f33128
 , @f33135
 , @f33137
 , @f33138
 , @f33143
 , @f33144
 , @f33145
 , @f33148
 , @f33153
 , @f33154
 , @f33155
 , @f33157
 , @f33163
 , @f33164
 , @f33165
 , @f33166
 , @f33167
 , @f33168
 , @f33203
 , @f33204
 , @f33205
 , @f33206
 , @f33207
 , @f33208
 , @f33217
 , @f33218
 , @f33225
 , @f33227
 , @f33228
 , @f33235
 , @f33237
 , @f33238
 , @f33243
 , @f33244
 , @f33245
 , @f33247
 , @f33248
 , @f33253
 , @f33254
 , @f33255
 , @f33257
 , @f33258
 , @f33263
 , @f33264
 , @f33265
 , @f33266
 , @f33267
 , @f33268
 , @f33277
 , @f33278
 , @f33305
 , @f33306
 , @f33307
 , @f33406
 , @f33407
 , @f33003
 , @f33004
 , @f33005
 , @f33006
 , @f33007
 , @f33008
 , @f36003
 , @f36004
 , @f41103
 , @f41113
 , @f41123
 , @f41133
 , @f41193
 , @f41203
 , @f41213
 , @f41223
 , @f41233
 , @f41243
 , @f41293
 , @f41003
 , @f42103
 , @f42113
 , @f42123
 , @f42133
 , @f42143
 , @f42193
 , @f42203
 , @f42213
 , @f42223
 , @f42233
 , @f42243
 , @f42293
 , @f42003
 , @f43103
 , @f43113
 , @f43123
 , @f43133
 , @f43143
 , @f43193
 , @f43203
 , @f43213
 , @f43223
 , @f43233
 , @f43293
 , @f43003
 , @f44003
 , @f44903
 , @f61003
 , @f62103
 , @f62153
 , @f62203
 , @f62303
 , @f62403
 , @f62503
 , @f62003
 , @f63103
 , @f63113
 , @f63123
 , @f63133
 , @f63203
 , @f63213
 , @f63223
 , @f63233
 , @f63243
 , @f63253
 , @f63263
 , @f63303
 , @f63503
 , @f63003
 , @f64003

 , @timestamp
)
SET 
   
   inn=@inn
 , name=convert(@name using utf8)
 , okpo=@okpo
 , okopf=@okopf
 , okfs=@okfs
 , okved=@okved
 , row_number= @row_number:=@row_number+1

 , f1110= if(383=@measure,@f11103,(1000 * @f11103))
 , f1120= if(383=@measure,@f11203,(1000 * @f11203))
 , f1130= if(383=@measure,@f11303,(1000 * @f11303))
 , f1140= if(383=@measure,@f11403,(1000 * @f11403))
 , f1150= if(383=@measure,@f11503,(1000 * @f11503))
 , f1160= if(383=@measure,@f11603,(1000 * @f11603))
 , f1170= if(383=@measure,@f11703,(1000 * @f11703))
 , f1180= if(383=@measure,@f11803,(1000 * @f11803))
 , f1190= if(383=@measure,@f11903,(1000 * @f11903))
 , f1100= if(383=@measure,@f11003,(1000 * @f11003))
 , f1210= if(383=@measure,@f12103,(1000 * @f12103))
 , f1220= if(383=@measure,@f12203,(1000 * @f12203))
 , f1230= if(383=@measure,@f12303,(1000 * @f12303))
 , f1240= if(383=@measure,@f12403,(1000 * @f12403))
 , f1250= if(383=@measure,@f12503,(1000 * @f12503))
 , f1260= if(383=@measure,@f12603,(1000 * @f12603))
 , f1200= if(383=@measure,@f12003,(1000 * @f12003))
 , f1600= if(383=@measure,@f16003,(1000 * @f16003))
 , f1310= if(383=@measure,@f13103,(1000 * @f13103))
 , f1320= if(383=@measure,@f13203,(1000 * @f13203))
 , f1340= if(383=@measure,@f13403,(1000 * @f13403))
 , f1350= if(383=@measure,@f13503,(1000 * @f13503))
 , f1360= if(383=@measure,@f13603,(1000 * @f13603))
 , f1370= if(383=@measure,@f13703,(1000 * @f13703))
 , f1300= if(383=@measure,@f13003,(1000 * @f13003))
 , f1410= if(383=@measure,@f14103,(1000 * @f14103))
 , f1420= if(383=@measure,@f14203,(1000 * @f14203))
 , f1430= if(383=@measure,@f14303,(1000 * @f14303))
 , f1450= if(383=@measure,@f14503,(1000 * @f14503))
 , f1400= if(383=@measure,@f14003,(1000 * @f14003))
 , f1510= if(383=@measure,@f15103,(1000 * @f15103))
 , f1520= if(383=@measure,@f15203,(1000 * @f15203))
 , f1530= if(383=@measure,@f15303,(1000 * @f15303))
 , f1540= if(383=@measure,@f15403,(1000 * @f15403))
 , f1550= if(383=@measure,@f15503,(1000 * @f15503))
 , f1500= if(383=@measure,@f15003,(1000 * @f15003))
 , f1700= if(383=@measure,@f17003,(1000 * @f17003))
 , f2110= if(383=@measure,@f21103,(1000 * @f21103))
 , f2120= if(383=@measure,@f21203,(1000 * @f21203))
 , f2100= if(383=@measure,@f21003,(1000 * @f21003))
 , f2210= if(383=@measure,@f22103,(1000 * @f22103))
 , f2220= if(383=@measure,@f22203,(1000 * @f22203))
 , f2200= if(383=@measure,@f22003,(1000 * @f22003))
 , f2310= if(383=@measure,@f23103,(1000 * @f23103))
 , f2320= if(383=@measure,@f23203,(1000 * @f23203))
 , f2330= if(383=@measure,@f23303,(1000 * @f23303))
 , f2340= if(383=@measure,@f23403,(1000 * @f23403))
 , f2350= if(383=@measure,@f23503,(1000 * @f23503))
 , f2300= if(383=@measure,@f23003,(1000 * @f23003))
 , f2410= if(383=@measure,@f24103,(1000 * @f24103))
 , f2421= if(383=@measure,@f24213,(1000 * @f24213))
 , f2430= if(383=@measure,@f24303,(1000 * @f24303))
 , f2450= if(383=@measure,@f24503,(1000 * @f24503))
 , f2460= if(383=@measure,@f24603,(1000 * @f24603))
 , f2400= if(383=@measure,@f24003,(1000 * @f24003))
 , f2510= if(383=@measure,@f25103,(1000 * @f25103))
 , f2520= if(383=@measure,@f25203,(1000 * @f25203))
 , f2500= if(383=@measure,@f25003,(1000 * @f25003))

;
show warnings;
select 2016 year, now() 'ok    load data.small.2016.txt', count(*) '' from finstat_csv;

select now() '', 'show bad inn {' as ' ';
select row_number, name, inn from finstat_csv where 10<>length(inn)\G
select now() '', 'show bad inn }' as ' ';
select now() '', 'show bad okved {' as ' ';
select row_number, name, inn, okved from finstat_csv where length(okved)>8\G
select now() '', 'show bad okved }' as ' ';

select now() '', 'find duplicated inn {' as ' ';
create temporary table duplicated_inn as
select count(*) count_inn, inn from finstat_csv group by inn having count_inn>1;
select * from duplicated_inn;
select now() '', 'find duplicated inn }' as ' ';



select now() '',  'insert into finstat_subject {' as ' ', concat('count:',count(*)) '   ' from finstat_subject;
insert into finstat_subject 
      (inn,name,okpo,okopf,okfs,okved)
select inn,name,okpo,okopf,okfs,okved
from finstat_csv
on duplicate key update
    name=finstat_csv.name
  , okpo=finstat_csv.okpo
  , okopf=finstat_csv.okopf
  , okfs=finstat_csv.okfs
  , okved=finstat_csv.okved
;
select now() '',  'insert into finstat_subject }' as ' ', concat('count:',count(*)) '   ' from finstat_subject;

explain select *
from finstat_csv
inner join finstat_subject on finstat_csv.inn=finstat_subject.inn\G

select now() '',  'insert into finstat {' as ' ', concat('count:',count(*)) '   ' from finstat;
insert into finstat
      (id_finstat_year_portion,  id_finstat_subject
      ,okpo,okopf,okfs,okved
 , f1110
 , f1120
 , f1130
 , f1140
 , f1150
 , f1160
 , f1170
 , f1180
 , f1190
 , f1100
 , f1210
 , f1220
 , f1230
 , f1240
 , f1250
 , f1260
 , f1200
 , f1600
 , f1310
 , f1320
 , f1340
 , f1350
 , f1360
 , f1370
 , f1300
 , f1410
 , f1420
 , f1430
 , f1450
 , f1400
 , f1510
 , f1520
 , f1530
 , f1540
 , f1550
 , f1500
 , f1700
 , f2110
 , f2120
 , f2100
 , f2210
 , f2220
 , f2200
 , f2310
 , f2320
 , f2330
 , f2340
 , f2350
 , f2300
 , f2410
 , f2421
 , f2430
 , f2450
 , f2460
 , f2400
 , f2510
 , f2520
 , f2500

      )
select @id_finstat_year_portion, id_finstat_subject
      ,finstat_csv.okpo,finstat_csv.okopf,finstat_csv.okfs,finstat_csv.okved
 , finstat_csv.f1110
 , finstat_csv.f1120
 , finstat_csv.f1130
 , finstat_csv.f1140
 , finstat_csv.f1150
 , finstat_csv.f1160
 , finstat_csv.f1170
 , finstat_csv.f1180
 , finstat_csv.f1190
 , finstat_csv.f1100
 , finstat_csv.f1210
 , finstat_csv.f1220
 , finstat_csv.f1230
 , finstat_csv.f1240
 , finstat_csv.f1250
 , finstat_csv.f1260
 , finstat_csv.f1200
 , finstat_csv.f1600
 , finstat_csv.f1310
 , finstat_csv.f1320
 , finstat_csv.f1340
 , finstat_csv.f1350
 , finstat_csv.f1360
 , finstat_csv.f1370
 , finstat_csv.f1300
 , finstat_csv.f1410
 , finstat_csv.f1420
 , finstat_csv.f1430
 , finstat_csv.f1450
 , finstat_csv.f1400
 , finstat_csv.f1510
 , finstat_csv.f1520
 , finstat_csv.f1530
 , finstat_csv.f1540
 , finstat_csv.f1550
 , finstat_csv.f1500
 , finstat_csv.f1700
 , finstat_csv.f2110
 , finstat_csv.f2120
 , finstat_csv.f2100
 , finstat_csv.f2210
 , finstat_csv.f2220
 , finstat_csv.f2200
 , finstat_csv.f2310
 , finstat_csv.f2320
 , finstat_csv.f2330
 , finstat_csv.f2340
 , finstat_csv.f2350
 , finstat_csv.f2300
 , finstat_csv.f2410
 , finstat_csv.f2421
 , finstat_csv.f2430
 , finstat_csv.f2450
 , finstat_csv.f2460
 , finstat_csv.f2400
 , finstat_csv.f2510
 , finstat_csv.f2520
 , finstat_csv.f2500

from finstat_csv
inner join finstat_subject on finstat_csv.inn=finstat_subject.inn;
select now() '',  'insert into finstat }' as ' ', concat('count:',count(*)) '   ' from finstat;

DROP TEMPORARY TABLE IF EXISTS finstat_csv;

UNLOCK TABLES;
