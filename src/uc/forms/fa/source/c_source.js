﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/fa/source/v_source.html'
	, 'forms/fa/model/src/h_0form1'
	, 'forms/fa/model/src/h_1form2'
	, 'forms/fa/model/src/h_2extra_accounts'
],
function (c_fastened, v_factors, h_form1, h_form2, h_2extra_accounts)
{
	return function ()
	{
		var controller = c_fastened(
			function (obj)
			{
				obj = (!obj || null == obj) ? { ряды: null } : { ряды: null == obj.value() ? null : obj.value().ряды };
				obj.form1 = h_form1;
				obj.form2 = h_form2;
				obj.extra_accounts = h_2extra_accounts;
				var html = v_factors(obj);
				return html;
			}
		);

		return controller;
	}
});