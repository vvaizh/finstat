﻿define([
	'tpl!forms/fa/source/v_source.html'
	, 'tpl!forms/fa/base/v_test_a4_portrait.html'
	, 'forms/fa/model/src/h_0form1'
	, 'forms/fa/model/src/h_1form2'
	, 'forms/fa/model/src/h_2extra_accounts'
]
, function (view, v_test_a4_album, h_form1, h_form2, h_2extra_accounts)
{
	return function ()
	{
		var res = {};

		res.Decode= function(json_txt)
		{
			var obj = JSON.parse(json_txt);
			if (obj.length)
				obj = { ряды: obj };
			var report_html = view({
				ряды: obj.ряды
				, form1: h_form1
				, form2: h_form2
				, extra_accounts: h_2extra_accounts
			});
			var layout_html = v_test_a4_album({});
			return layout_html.replace('test-content', report_html);
		}

		return res;
	}

});
