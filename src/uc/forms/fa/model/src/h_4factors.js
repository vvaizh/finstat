﻿define(function ()
{
	var Коэффициент_абсолютной_ликвидности = {
		title: 'Коэффициент абсолютной ликвидности'
		, строка: 'kabl'
		, formula: 'fnaib_likv_ob_act / ftekushie_obiazatelstva'
	};

	var Коэффициент_текущей_ликвидности = {
		title: 'Коэффициент текущей ликвидности'
		, строка: 'ktl'
		, formula: 'flikv_act / ftekushie_obiazatelstva'
	};

	var Коэффициент_обеспеченности = {
		title: 'Показатель обеспеченности обязательств активами'
		, строка: 'kob'
		, formula: 'fkor_vneob_act / ftekushie_obiazatelstva'
	};

	var Плановый_срок_расчётов_по_текущим = {
		title: 'Платежеспособность по текущим обязательствам'
		, строка: 'srok_rasch_po_tek'
		, formula: 'ftekushie_obiazatelstva / fsredne_mes_viruchka'
	};

	var Автономия = {
		title: 'Автономия (финансовая независимость)'
		, строка: 'avtonomia'
		, formula: 'fsobstven_sredstva / fbalans_total'
	};

	var Обеспеченность_оборотными_средстваим = {
		title: 'Обеспеченность оборотными средствами'
		, строка: 'obespechennost_oborotnimi'
		, formula: '( fsobstven_sredstva - fkor_vneob_act ) / fob_act'
	};

	var Просроченная_кредиторская_задолженность = {
		title: 'Доля просроченной кредиторской задолженности'
		, строка: 'prosr_kred'
		, formula: 'f1520 / f1700'
	};

	var Дебиторка_в_активах = {
		title: 'Отношение дебиторской задолженности к активам'
		, строка: 'debitorka_v_activah'
		, formula: 'f1230 / fbalans_total'
	};

	var Рентабельность_активов = {
		title: 'Рентабельность активов'
		, строка: 'rentabelnost_aktivov'
		, formula: 'fchistaia_pribil / fbalans_total'
	};

	var Норма_чистой_прибыли = {
		title: 'Норма чистой прибыли'
		, строка: 'norma_chistoi_pribili'
		, formula: 'fchistaia_pribil / fviruchka_netto'
	};

	var Показатели = [
		  Коэффициент_абсолютной_ликвидности
		, Коэффициент_текущей_ликвидности
		, Коэффициент_обеспеченности
		, Плановый_срок_расчётов_по_текущим

		, Автономия
		, Обеспеченность_оборотными_средстваим
		, Просроченная_кредиторская_задолженность
		, Дебиторка_в_активах

		, Рентабельность_активов
		, Норма_чистой_прибыли
	];
	return Показатели;
});