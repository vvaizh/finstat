defined(function(){

/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Formula_to_function.js.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


var calculate= {

/* порядок перерасчёта: 0: */

  "balans_total":                  function (r) { return r.f1600; }
, "kor_vneob_act":                 function (r) { return r.f1100 + r.f1150 + r.f1160 + r.f1170 + r.f1190; }
, "ob_act":                        function (r) { return r.f1200; }
, "deb_dolg":                      function (r) { return r.f1230; }
, "likv_act":                      function (r) { return r.f1250 + r.f1240 + r.f1260 + r.f1230; }
, "naib_likv_ob_act":              function (r) { return r.f1250 + r.f1240 - r.f1320; }
, "sobstven_sredstva":             function (r) { return r.f1300 + r.f1530 + r.f1540 + r.f1430 + r.fkap_zatrat_arenda - r.f1320; }
, "obyazatelstva":                 function (r) { return r.f1450 + r.f1410 + r.f1510 + r.f1520 + r.f1550; }
, "dolgosrochnie_obiazatelstva":   function (r) { return r.f1450 + r.f1410; }
, "tekushie_obiazatelstva":        function (r) { return r.f1510 + r.f1520 + r.f1550; }
, "viruchka_netto":                function (r) { return r.f2110; }
, "valovaia_viruchka":             function (r) { return r.f2110 + r.fnds_i_drugie_obiaz_platezh; }
, "sredne_mes_viruchka":           function (r) { return r.fvalovaia_viruchka / r.fmes_v_periode; }
, "chistaia_pribil":               function (r) { return r.f2400; }

/* порядок перерасчёта: 1: */

, "kabl":                          function (r) { return r.fnaib_likv_ob_act / r.ftekushie_obiazatelstva; }
, "ktl":                           function (r) { return r.flikv_act / r.ftekushie_obiazatelstva; }
, "kob":                           function (r) { return r.fkor_vneob_act / r.ftekushie_obiazatelstva; }
, "srok_rasch_po_tek":             function (r) { return r.ftekushie_obiazatelstva / r.fsredne_mes_viruchka; }
, "avtonomia":                     function (r) { return r.fsobstven_sredstva / r.fbalans_total; }
, "obespechennost_oborotnimi":     function (r) { return ( r.fsobstven_sredstva - r.fkor_vneob_act ) / r.fob_act; }
, "prosr_kred":                    function (r) { return r.f1520 / r.f1700; }
, "debitorka_v_activah":           function (r) { return r.f1230 / r.fbalans_total; }
, "rentabelnost_aktivov":          function (r) { return r.fchistaia_pribil / r.fbalans_total; }
, "norma_chistoi_pribili":         function (r) { return r.fchistaia_pribil / r.fviruchka_netto; }

};

return calculate;

});
