/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Prepare_table.sql.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */




LOAD DATA LOCAL INFILE 
  @@finstat_opendata_filename_to_load 
INTO TABLE finstat 

FIELDS TERMINATED BY ';'
ESCAPED BY '\\'
LINES TERMINATED BY '\r\n'
(

   @name
 , @okpo
 , @okopf
 , @okfs
 , @okved
 , @inn
 , @measure
 , @type

)
SET 

   inn=@inn
 , year=@year
 , type=@type
 , okpo=@okpo
 , okopf=@okopf
 , okfs=@okfs
 , okved=@okved

;
