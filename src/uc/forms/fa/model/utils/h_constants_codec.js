﻿define(function ()
{
	var helper = {};

	helper.PhpCommentsHeader = function (filename)
	{
		var res = '';

		res += '/* ВНИМАНИЕ! */\r\n';
		res += '/* данный файл генерируется автоматически */\r\n'
		res += '/* при помощи модуля ' + filename + '.js */\r\n'
		res += '/* НЕ редактируйте этот файл вручную для избежания конфликтов! */\r\n'
		res += '\r\n\r\n';

		return res;
	}

	return helper;
});
