﻿define([
	'forms/base/codec/codec'
	, 'forms/fa/model/utils/h_constants_codec'
	, 'forms/fa/model/utils/h_zcollector'
],
function (BaseCodec, h_constants_codec, описание_строк)
{
	return function ()
	{
		var codec = BaseCodec();
		codec.Encode = function (data)
		{
			var res = '';

			res += h_constants_codec.PhpCommentsHeader('codec.Prepare_table.sql');

			res += 'alter table finstat\r\n';
			for (var i = 0; i < описание_строк.по_порядку.length; i++)
			{
				var s = описание_строк.по_порядку[i];
				if (('форма1' == s.источник || 'форма2' == s.источник)
					&& !s.пропущено_в_росстате)
				{
					res += (0 == i) ? '  ' : ' ,';
					res += ' add f' + s.строка + ' bigint not null\r\n';
				}
			}
			res += ';\r\n';

			return res;
		}

		return codec;
	}
});
