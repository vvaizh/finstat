﻿define([
	'forms/base/codec/codec'
	, 'forms/fa/model/utils/h_constants_codec'
	, 'forms/fa/model/utils/h_zcollector'
	, 'forms/fa/model/utils/h_formula'
],
function (BaseCodec, h_constants_codec, описание_строк, h_formula)
{
	return function ()
	{
		var codec = BaseCodec();
		codec.Encode = function (data)
		{
			var res = 'defined(function(){\r\n\r\n';

			res += h_constants_codec.PhpCommentsHeader('codec.Formula_to_function.js');

			var count = 0;
			for (var s in описание_строк.по_строке)
				count++;

			if (описание_строк.по_порядку.length != count)
				res+= 'ВНИМАНИЕ! количество строк по названиям:' + count + ' не равно количеству строк по порядку:' + описание_строк.по_порядку.length;

			res += 'var calculate= {\r\n';

			var count = 0;
			for (var ilevel = 0; ilevel < описание_строк.по_порядку_пересчёта.length; ilevel++)
			{
				res += '\r\n/* порядок перерасчёта: ' + ilevel + ': */\r\n\r\n';
				var строки = описание_строк.по_порядку_пересчёта[ilevel];
				for (var iline = 0; iline < строки.length; iline++)
				{
					var s = строки[iline];
					var prefix = (0 == count) ? '  ' : ', ';
					prefix += '"' + s.строка + '":';
					while (prefix.length < 35)
						prefix += ' ';
					var function_body = h_formula.заменить_строки(s.formula, function (s) { return 'r.' + s; });
					res += prefix + 'function (r) { return ' + function_body + '; }\r\n';
					count++;

					WScript.Echo('строка ' + s.строка);
					WScript.Echo('    формула ' + s.formula);
					WScript.Echo('    порядок пересчёта: ' + s.порядок_пересчёта);
					WScript.Echo('    зависит от строк:');
					var args = h_formula.строки_аргументы(s.formula);
					for (var i = 0; i < args.length; i++)
					{
						WScript.Echo('        ' + args[i] + ':');
						var s_arg = описание_строк.по_строке[args[i]];
						WScript.Echo('              ' + ((0 != s_arg.порядок_пересчёта && !s_arg.порядок_пересчёта) ? '' : s_arg.порядок_пересчёта + ' ') + s_arg.title);
						if (s_arg.порядок_пересчёта && s_arg.порядок_пересчёта >= s.порядок_пересчёта)
							WScript.Echo('ВНИМАНИЕ! обнаружена циклическая зависимость!');
					}
				}
			}

			res += '\r\n};\r\n\r\n';
			res += 'return calculate;\r\n\r\n';
			res += '});\r\n';

			return res;
		}

		return codec;
	}
});
