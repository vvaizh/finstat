﻿define([
	'forms/base/codec/codec'
	, 'forms/fa/model/utils/h_constants_codec'
	, 'forms/fa/model/utils/h_zcollector'
	, 'tpl!forms/fa/model/utils/tpl.Load_csv.sql'
	, 'forms/fa/model/src/h_rosstat_indicators'
],
	function (BaseCodec, h_constants_codec, описание_строк, tpl_Load_csv, h_rosstat_indicators)
{
	return function ()
	{
		var codec = BaseCodec();
		codec.Encode = function (data)
		{
			var res = '';

			res += h_constants_codec.PhpCommentsHeader('codec.Prepare_table.sql');

			res += tpl_Load_csv({
				  filename: data.filename
				, year: data.year
				, csv_indicators: h_rosstat_indicators
				, описание_строк: описание_строк
			})

			return res;
		}

		return codec;
	}
});
