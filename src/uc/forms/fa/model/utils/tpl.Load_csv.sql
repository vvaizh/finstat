﻿<% 
var csv_indicators= !obj || null==obj || !obj.csv_indicators || null==obj.csv_indicators ? [] : obj.csv_indicators;
var строки_по_порядку= !obj || null==obj || !obj.описание_строк || null==obj.описание_строк ? [] : obj.описание_строк.по_порядку;
var year= obj.year;

var bad_year_inn_name_row_number= {
    '2018':[
        {
            inn:'7702011831'
            ,row_number:1312876
            ,name:'ОРЕХОВО-ЗУЕВСКИЙ ФИЛИАЛ ОБЩЕСТВА С ОГРАНИЧЕННОЙ ОТВЕТСТВЕННОСТЬЮ КОНЦЕРН "СТАРТ-ЦБС" "ОПТОВО-ПРОИЗВОДСТВЕННАЯ БАЗА'
        }
    ]
};

%>

LOCK TABLES finstat WRITE, finstat_subject WRITE, finstat_year_portion WRITE;

insert into finstat_year_portion
set year= <%= year %>, FileName= "<%= obj.filename %>";
set @id_finstat_year_portion= last_insert_id();

DROP TEMPORARY TABLE IF EXISTS finstat_csv;
CREATE TEMPORARY TABLE finstat_csv
(
	inn                   VARCHAR(10)    NOT NULL,
	name                  TEXT           NOT NULL,
	okpo                  VARCHAR(8)     NOT NULL,
	okopf                 VARCHAR(5)     NOT NULL,
	okfs                  VARCHAR(2)     NOT NULL,
	okved                 VARCHAR(9)     NOT NULL,

	row_number           INT
	<% if (bad_year_inn_name_row_number[year]) { %>, PRIMARY KEY (row_number)<% } %>

<% 
	for (var i= 0; i< строки_по_порядку.length; i++) 
	{ 
		var s= строки_по_порядку[i];
		if (('форма1' == s.источник || 'форма2' == s.источник)
			&& !s.пропущено_в_росстате) 
		{
			%> , f<%= s.строка %> BIGINT NOT NULL
<%		}
	} 
%>
)
;

set @row_number = 0;
select <%= year %> year, now() 'start load <%= obj.filename %>';

LOAD DATA LOCAL INFILE 
  '<%= obj.filename %>'
INTO TABLE finstat_csv 

CHARACTER SET cp1251
FIELDS TERMINATED BY ';'
ESCAPED BY '$'
LINES TERMINATED BY '\r\n'
(

   @name
 , @okpo
 , @okopf
 , @okfs
 , @okved
 , @inn
 , @measure
 , @type

<%	for (var i= 0; i< csv_indicators.length; i++)
	{ %> , @f<%= csv_indicators[i] %>
<%	} %>
 , @timestamp
)
SET 
   
   inn=@inn
 , name=convert(@name using utf8)
 , okpo=@okpo
 , okopf=@okopf
 , okfs=@okfs
 , okved=@okved
 , row_number= @row_number:=@row_number+1

<% 
	for (var i= 0; i< строки_по_порядку.length; i++) 
	{ 
		var s= строки_по_порядку[i];
		if (('форма1' == s.источник || 'форма2' == s.источник)
			&& !s.пропущено_в_росстате) 
		{
			%> , f<%= s.строка %>= if(383=@measure,@f<%= s.строка %>3,(1000 * @f<%= s.строка %>3))
<%		}
	} 
%>
;
show warnings;
select <%= year %> year, now() 'ok    load <%= obj.filename %>', count(*) '' from finstat_csv;

select now() '', 'show bad inn {' as ' ';
select row_number, name, inn from finstat_csv where 10<>length(inn)\G
select now() '', 'show bad inn }' as ' ';
select now() '', 'show bad okved {' as ' ';
select row_number, name, inn, okved from finstat_csv where length(okved)>8\G
select now() '', 'show bad okved }' as ' ';

select now() '', 'find duplicated inn {' as ' ';
create temporary table duplicated_inn as
select count(*) count_inn, inn from finstat_csv group by inn having count_inn>1;
select * from duplicated_inn;
select now() '', 'find duplicated inn }' as ' ';

<%
if (bad_year_inn_name_row_number[year])
{
    var year_duplicates= bad_year_inn_name_row_number[year];
%>
select now() '', 'show duplicated inn {' as ' ';
select finstat_csv.inn, row_number, name
from finstat_csv 
inner join duplicated_inn on finstat_csv.inn=duplicated_inn.inn;
select now() '', 'show duplicated inn }' as ' ';
select now() '', 'delete duplicated inn (row_number=<% 
 for (var i= 0; i < year_duplicates.length; i++) { var d= year_duplicates[i]; %><%= (0==i? '' : ',' )+ d.row_number %><% } 
%>) {' as ' ';
delete from finstat_csv
where row_number in (<% for (var i= 0; i < year_duplicates.length; i++) { var d= year_duplicates[i]; %><%= (0==i? '' : ',' )+ d.row_number %><% } %>);
select now() '', 'delete duplicated inn }' as ' ';
select now() '', 'find duplicated inn again {' as ' ';
select count(*) count_inn, inn from finstat_csv group by inn having count_inn>1;
select now() '', 'find duplicated inn again }' as ' ';
<%
}
%>

select now() '',  'insert into finstat_subject {' as ' ', concat('count:',count(*)) '   ' from finstat_subject;
insert into finstat_subject 
      (inn,name,okpo,okopf,okfs,okved)
select inn,name,okpo,okopf,okfs,okved
from finstat_csv
on duplicate key update
    name=finstat_csv.name
  , okpo=finstat_csv.okpo
  , okopf=finstat_csv.okopf
  , okfs=finstat_csv.okfs
  , okved=finstat_csv.okved
;
select now() '',  'insert into finstat_subject }' as ' ', concat('count:',count(*)) '   ' from finstat_subject;

explain select *
from finstat_csv
inner join finstat_subject on finstat_csv.inn=finstat_subject.inn\G

select now() '',  'insert into finstat {' as ' ', concat('count:',count(*)) '   ' from finstat;
insert into finstat
      (id_finstat_year_portion,  id_finstat_subject
      ,okpo,okopf,okfs,okved
<% 
	for (var i= 0; i< строки_по_порядку.length; i++) 
	{ 
		var s= строки_по_порядку[i];
		if (('форма1' == s.источник || 'форма2' == s.источник)
			&& !s.пропущено_в_росстате) 
		{
			%> , f<%= s.строка %>
<%		}
	} 
%>
      )
select @id_finstat_year_portion, id_finstat_subject
      ,finstat_csv.okpo,finstat_csv.okopf,finstat_csv.okfs,finstat_csv.okved
<% 
	for (var i= 0; i< строки_по_порядку.length; i++) 
	{ 
		var s= строки_по_порядку[i];
		if (('форма1' == s.источник || 'форма2' == s.источник)
			&& !s.пропущено_в_росстате) 
		{
			%> , finstat_csv.f<%= s.строка %>
<%		}
	} 
%>
from finstat_csv
inner join finstat_subject on finstat_csv.inn=finstat_subject.inn;
select now() '',  'insert into finstat }' as ' ', concat('count:',count(*)) '   ' from finstat;

DROP TEMPORARY TABLE IF EXISTS finstat_csv;

UNLOCK TABLES;
