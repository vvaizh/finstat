﻿define(function ()
{
	var helper = {};

	helper.заменить_строки = function (formula, замена)
	{
		var parts = formula.split(' ');
		var res = '';
		for (var i = 0; i < parts.length; i++)
		{
			if (0 != i)
				res += ' ';
			var part = parts[i];
			res += ('f' != part.charAt(0)) ? part : замена(part);
		}
		return res;
	};

	helper.строки_аргументы = function (formula)
	{
		var res = [];
		var res_by_name = {};
		var parts = formula.split(' ');
		for (var i = 0; i < parts.length; i++)
		{
			var part = parts[i];
			if ('f' == part.charAt(0))
			{
				if (!res_by_name[part])
				{
					res_by_name[part] = part;
					res.push(part);
				}
			}
		}
		return res;
	}

	return helper;
});
