﻿define([
	'tpl!forms/fa/factors/v_factors.html'
	, 'tpl!forms/fa/base/v_test_a4_album.html'
]
	, function (view, v_test_a4_album)
{
	return function ()
	{
		var res = {};

		res.Decode= function(json_txt)
		{
			var obj = JSON.parse(json_txt);
			if (obj.length)
				obj = { ряды: obj };
			var report_html = view({ ряды: obj.ряды });
			var layout_html = v_test_a4_album({});
			report_html = 'Коэффициенты финансово хозяйственной деятельности '
				+ obj.Наименование
				+ ':' + report_html;
			return layout_html.replace('test-content', report_html);
		}

		return res;
	}

});
