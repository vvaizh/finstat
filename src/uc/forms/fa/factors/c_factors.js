﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/fa/factors/v_factors.html'
],
function (c_fastened, v_factors)
{
	return function ()
	{
		var controller = c_fastened(
			function (obj)
			{
				obj = (!obj || null == obj) ? { ряды: null } : { ряды: null == obj.value() ? null : obj.value().ряды };
				var html = v_factors(obj);
				return html;
			}
		);

		return controller;
	}
});