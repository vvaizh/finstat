define([
	  'forms/base/h_spec'
]
, function (h_spec)
{
	var fa_specs = {

		controller: {
			"factors":
			{
				path: 'forms/fa/factors/c_factors'
				, title: 'коэффиценты с периодами по вертикали'
				, keywords: 'коэффициенты'
			}
			, "source":
			{
				path: 'forms/fa/source/c_source'
				, title: 'исходные данные в виде балансов на А4'
				, keywords: 'форма 1 1 '
			}
			, "ind_factors":
			{
				path: 'forms/fa/ind_factors/c_ind_factors'
				, title: 'показатели для коэффициентов и коэффициенты на А4'
			}
		}

		, content: {
			"factors-example1": { path: 'txt!forms/fa/factors/tests/contents/example1.json.txt' }
			, "factors-example2-no-fixes": { path: 'txt!forms/fa/factors/tests/contents/example2-no-fixes.json.txt' }
			, "factors-example3-min": { path: 'txt!forms/fa/factors/tests/contents/example3-min.json.txt' }

			, "source-example1-min": { path: 'txt!forms/fa/source/tests/contents/example1-min.json.txt' }

			, "ind_factors-example1-min": { path: 'txt!forms/fa/ind_factors/tests/contents/example1-min.json.txt' }
		}

	};

	return h_spec.combine(fa_specs);
});