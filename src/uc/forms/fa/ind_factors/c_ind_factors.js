﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/fa/ind_factors/v_ind_factors.html'
	, 'forms/fa/model/src/h_3indicators'
],
function (c_fastened, tpl, h_indicators)
{
	return function ()
	{
		var controller = c_fastened(
			function (obj)
			{
				obj = (!obj || null == obj) ? { ряды: null } : { ряды: null == obj.value() ? null : obj.value().ряды };
				obj.indicators = h_indicators;
				var html = tpl(obj);
				return html;
			}
		);

		return controller;
	}
});