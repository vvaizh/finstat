﻿define([
	'tpl!forms/fa/ind_factors/v_ind_factors.html'
	, 'tpl!forms/fa/base/v_test_a4_portrait.html'
	, 'forms/fa/model/src/h_3indicators'
]
, function (view, v_test_a4, h_indicators)
{
	return function ()
	{
		var res = {};

		res.Decode= function(json_txt)
		{
			var obj = JSON.parse(json_txt);
			if (obj.length)
				obj = { ряды: obj };
			var report_html = view({
				ряды: obj.ряды
				, indicators: h_indicators
			});
			var layout_html = v_test_a4({});
			report_html = 'Анализ финансово хозяйственной деятельности '
				+ obj.Наименование
				+ ':' + report_html;
			return layout_html.replace('test-content', report_html);
		}

		return res;
	}

});
