start_store_lines_as typeahead_fields_1

  js wbt.SetModelFieldValue("first", "Alas");
  js wbt.CheckModelFieldValue("first", "Alaska");

stop_store_lines

start_store_lines_as typeahead_fields_2

  js wbt.SetModelFieldValue("first", "Pen");
  js wbt.CheckModelFieldValue("first", "Pennsylvania");

stop_store_lines

start_store_lines_as typeahead_fields_3

  js wbt.SetModelFieldValue("first", "Delaw");
  js wbt.CheckModelFieldValue("first", "Delaware");

stop_store_lines

