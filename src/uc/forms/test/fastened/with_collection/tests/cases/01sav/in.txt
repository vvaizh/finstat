include ..\..\..\..\..\..\wbt.lib.txt quiet
include ..\..\..\..\..\fastened\simple\tests\cases\in.lib.txt quiet
execute_javascript_stored_lines add_wbt_std_functions
wait_text "+"
shot_check_png ..\..\shots\00new.png

js wbt.SetModelFieldValue("Наименование", "ООО Роги и ноги");

click_text "+"
shot_check_png ..\..\shots\01sav-add.png
je wbt.Model_selector_prefix= '*[array-index="0"] ';
js wbt.SetModelFieldValue("", "1");

click_text "+"
je wbt.Model_selector_prefix= '*[array-index="1"] ';
js wbt.SetModelFieldValue("", "2");

shot_check_png ..\..\shots\01sav.png

wait_click_full_text "Сохранить отредактированную модель"
dump_js wbt_controller_GetFormContentTextArea ..\..\contents\01edt-2.json.result.txt
exit