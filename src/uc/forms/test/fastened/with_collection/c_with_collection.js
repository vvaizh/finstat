﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/fastened/with_collection/e_with_collection.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);

		return controller;
	}
});