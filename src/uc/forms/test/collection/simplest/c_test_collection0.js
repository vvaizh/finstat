﻿define([
	  'forms/base/fastened/c_fastened'
	, 'tpl!forms/test/collection/simplest/e_test_collection0.html'
],
function (c_fastened, tpl)
{
	return function ()
	{
		var controller = c_fastened(tpl);
		return controller;
	}
});