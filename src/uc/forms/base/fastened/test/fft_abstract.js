﻿define(function ()
{
	return function ()
	{
		var fastened_field_tester =
		{
			match: function (dom_item, tag_name, fc_type) { return false; }
			, set_value:   function (dom_item, value) { }
			, check_value: function (dom_item, value, instead_of_set) { }
		};
		return fastened_field_tester;
	}
});