﻿define([
	  'forms/base/fastened/test/fft_abstract'
]
, function (fft_abstract)
{
	var fastened_checkbox_tester = fft_abstract();

	fastened_checkbox_tester.match = function (dom_item, tag_name, fc_type)
	{
		return 'checkbox' == dom_item.attr('type');
	}

	fastened_checkbox_tester.check_value = function (dom_item, value)
	{
		var evalue = dom_item.attr('checked');
		if (value == evalue)
		{
			return null; // ' ok, value "' + value + '" is checked';
		}
		else
		{
			return ' value "' + evalue + '"! is WRONG!!!!!! should be "' + value + '"';
		}
	}

	fastened_checkbox_tester.set_value = function (dom_item, value)
	{
		dom_item.attr('checked', value).change();
		return 'set value "' + value + '"';
	}

	return fastened_checkbox_tester;
});