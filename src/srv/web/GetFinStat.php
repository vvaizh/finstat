<?

require_once '../assets/config.php';
require_once '../assets/helpers/db.php';
require_once '../assets/helpers/json.php';
require_once '../assets/helpers/log.php';

mb_internal_encoding("utf-8");
mb_http_output( "UTF-8" );
mb_http_input( "UTF-8" );

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: POST, GET, OPTIONS');

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

try
{
	global $_GET;
	$inn= !isset($_GET['inn']) ? '' : $_GET['inn'];

	if (''==$inn)
		throw new Exception('skipped mandatory GET parameters inn!');

	$txt_query= "select finstat_year_portion.year, finstat_subject.name, finstat.*
	from finstat 
	inner join finstat_subject on finstat.id_finstat_subject=finstat_subject.id_finstat_subject
	inner join finstat_year_portion on finstat_year_portion.id_finstat_year_portion=finstat.id_finstat_year_portion
	where inn=?";
	$rows= execute_query($txt_query,array('s',$inn));

	$res= array('inn'=>$inn);

	if (null!=$rows && count($rows)>0)
		$res['name']= $rows[0]->name;
	$res['ряды']= $rows;

	header('Content-Type: application/json');
	echo nice_json_encode($res);
}
catch (Exception $exception)
{
	header("HTTP/1.1 500 Internal Server Error");
	write_to_log('Unhandled exception occurred: ' . get_class($exception) . ' - ' . $exception->getMessage());
	write_to_log('$_GET:');
	write_to_log($_GET);
	throw new Exception("can not execute GetFinStat!");
}

