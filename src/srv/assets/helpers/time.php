<?php

require_once '../assets/helpers/log.php';

function safe_store_test_time()
{
	global $_SESSION;
	if (isset($_GET['use-test-time']) && ''!=$_GET['use-test-time'])
	{
		$res= date_create_from_format('Y-m-d\TH:i:s',$_GET['use-test-time']);
		$_SESSION['current-test-time']= date_format($res,'Y-m-d\TH:i:s');
		$_GET['use-test-time']= '';
	}
}

function safe_date_create()
{
	global $_SESSION;
	if (isset($_GET['use-test-time']) && ''!=$_GET['use-test-time'])
	{
		$res= date_create_from_format('Y-m-d\TH:i:s',$_GET['use-test-time']);
		$_SESSION['current-test-time']= date_format($res,'Y-m-d\TH:i:s');
		$_GET['use-test-time']= '';
		return $res;
	}
	else if (isset($_SESSION['current-test-time']))
	{
		$res= date_create_from_format('Y-m-d\TH:i:s',$_SESSION['current-test-time']);
		$res= date_add($res, date_interval_create_from_date_string('1 second'));
		$_SESSION['current-test-time']= date_format($res,'Y-m-d\TH:i:s');
		return $res;
	}
	else
	{
		return date_create();
	}
}