@set DBName=finstatdevel
@set MYSQLHOST="localhost"
@set MYSQLDIR=C:\Program Files\MySQL\MySQL Server 5.7\bin\
@set DUMP_OPTIONS=--hex-blob --default-character-set=utf8 --set-charset --routines --no-autocommit --extended-insert

@echo *******************************************************

@echo prepare create_sql
@call %~dp0\create_sql.bat

@echo prepare empty database %DBName%
@set SQL_DROP_CREATE=drop database %DBName%; create database %DBName% default character set utf8;
@echo %SQL_DROP_CREATE% | %~dp0\run_mysql.bat
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\create.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo execute post_create.sql
@call %~dp0\run_mysql.bat < %~dp0\sql\post_create.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!

@echo %time% execute alter_finstat_add_indicators.etalon.sql {
@call %~dp0\run_mysql.bat < %~dp0\sql\built\alter_finstat_add_indicators.etalon.sql
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!
@echo %time%      ok alter_finstat_add_indicators.etalon.sql }

@call :prepare-backup empty

exit

@call :load-test-data 2016
@call :load-test-data 2017
@call :load-test-data 2018

@call :prepare-backup test

@exit

@rem -----------------------------------------------------------
:load-test-data
@set TEST_DATA_YEAR=%1
@echo load %TEST_DATA_YEAR%\data.small.txt {
@copy %~dp0\..\opendata\%TEST_DATA_YEAR%\data.small.txt %~dp0\sql\built\data.txt
@copy %~dp0\..\opendata\%TEST_DATA_YEAR%\set_year_to_load.sql + %~dp0\sql\built\finstat_load.etalon.sql %~dp0\sql\built\finstat_load.sql
@pushd %~dp0\sql\built\
@call %~dp0\run_mysql.bat < %~dp0\sql\built\finstat_load.sql
@popd 
@echo   ok %TEST_DATA_YEAR%\data.small.txt }
@exit /B

@rem -----------------------------------------------------------
:prepare-backup
@set BACKUP_NAME=%1
@echo %time% get backups\%BACKUP_NAME% {
@call "%MYSQLDIR%\mysqldump.exe" --defaults-extra-file=%~dp0\mysql.conf --host=%MYSQLHOST% %DUMP_OPTIONS% --result-file=%~dp0\backups\%BACKUP_NAME%.sql %DBName%
@if NOT 0==%ERRORLEVEL% exit rem stop on ERROR!
@echo %time%  ok backups\%BACKUP_NAME% }
@exit /B