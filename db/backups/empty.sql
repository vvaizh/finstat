-- MySQL dump 10.13  Distrib 5.7.28, for Win64 (x86_64)
--
-- Host: localhost    Database: finstatdevel
-- ------------------------------------------------------
-- Server version	5.7.28-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `finstat`
--

DROP TABLE IF EXISTS `finstat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finstat` (
  `id_finstat` int(11) NOT NULL AUTO_INCREMENT,
  `id_finstat_subject` int(11) DEFAULT NULL,
  `id_finstat_year_portion` int(11) NOT NULL,
  `okpo` varchar(8) NOT NULL,
  `okopf` varchar(5) NOT NULL,
  `okfs` varchar(2) NOT NULL,
  `okved` varchar(8) NOT NULL,
  `f1110` bigint(20) NOT NULL,
  `f1120` bigint(20) NOT NULL,
  `f1130` bigint(20) NOT NULL,
  `f1140` bigint(20) NOT NULL,
  `f1150` bigint(20) NOT NULL,
  `f1160` bigint(20) NOT NULL,
  `f1170` bigint(20) NOT NULL,
  `f1180` bigint(20) NOT NULL,
  `f1190` bigint(20) NOT NULL,
  `f1100` bigint(20) NOT NULL,
  `f1210` bigint(20) NOT NULL,
  `f1220` bigint(20) NOT NULL,
  `f1230` bigint(20) NOT NULL,
  `f1240` bigint(20) NOT NULL,
  `f1250` bigint(20) NOT NULL,
  `f1260` bigint(20) NOT NULL,
  `f1200` bigint(20) NOT NULL,
  `f1600` bigint(20) NOT NULL,
  `f1310` bigint(20) NOT NULL,
  `f1320` bigint(20) NOT NULL,
  `f1340` bigint(20) NOT NULL,
  `f1350` bigint(20) NOT NULL,
  `f1360` bigint(20) NOT NULL,
  `f1370` bigint(20) NOT NULL,
  `f1300` bigint(20) NOT NULL,
  `f1410` bigint(20) NOT NULL,
  `f1420` bigint(20) NOT NULL,
  `f1430` bigint(20) NOT NULL,
  `f1450` bigint(20) NOT NULL,
  `f1400` bigint(20) NOT NULL,
  `f1510` bigint(20) NOT NULL,
  `f1520` bigint(20) NOT NULL,
  `f1530` bigint(20) NOT NULL,
  `f1540` bigint(20) NOT NULL,
  `f1550` bigint(20) NOT NULL,
  `f1500` bigint(20) NOT NULL,
  `f1700` bigint(20) NOT NULL,
  `f2110` bigint(20) NOT NULL,
  `f2120` bigint(20) NOT NULL,
  `f2100` bigint(20) NOT NULL,
  `f2210` bigint(20) NOT NULL,
  `f2220` bigint(20) NOT NULL,
  `f2200` bigint(20) NOT NULL,
  `f2310` bigint(20) NOT NULL,
  `f2320` bigint(20) NOT NULL,
  `f2330` bigint(20) NOT NULL,
  `f2340` bigint(20) NOT NULL,
  `f2350` bigint(20) NOT NULL,
  `f2300` bigint(20) NOT NULL,
  `f2410` bigint(20) NOT NULL,
  `f2421` bigint(20) NOT NULL,
  `f2430` bigint(20) NOT NULL,
  `f2450` bigint(20) NOT NULL,
  `f2460` bigint(20) NOT NULL,
  `f2400` bigint(20) NOT NULL,
  `f2510` bigint(20) NOT NULL,
  `f2520` bigint(20) NOT NULL,
  `f2500` bigint(20) NOT NULL,
  PRIMARY KEY (`id_finstat`),
  UNIQUE KEY `by_subject_year_portion` (`id_finstat_subject`,`id_finstat_year_portion`),
  KEY `by_finstat_subject` (`id_finstat_subject`),
  KEY `by_finstat_year_portion` (`id_finstat_year_portion`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finstat`
--

LOCK TABLES `finstat` WRITE;
/*!40000 ALTER TABLE `finstat` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `finstat` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `finstat_subject`
--

DROP TABLE IF EXISTS `finstat_subject`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finstat_subject` (
  `id_finstat_subject` int(11) NOT NULL AUTO_INCREMENT,
  `inn` varchar(10) NOT NULL,
  `name` text NOT NULL,
  `okpo` varchar(8) NOT NULL,
  `okopf` varchar(5) NOT NULL,
  `okfs` varchar(2) NOT NULL,
  `okved` varchar(8) NOT NULL,
  PRIMARY KEY (`id_finstat_subject`),
  UNIQUE KEY `by_inn` (`inn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finstat_subject`
--

LOCK TABLES `finstat_subject` WRITE;
/*!40000 ALTER TABLE `finstat_subject` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `finstat_subject` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Table structure for table `finstat_year_portion`
--

DROP TABLE IF EXISTS `finstat_year_portion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `finstat_year_portion` (
  `id_finstat_year_portion` int(11) NOT NULL AUTO_INCREMENT,
  `TimeLoaded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `FileName` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  PRIMARY KEY (`id_finstat_year_portion`),
  UNIQUE KEY `by_year` (`year`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finstat_year_portion`
--

LOCK TABLES `finstat_year_portion` WRITE;
/*!40000 ALTER TABLE `finstat_year_portion` DISABLE KEYS */;
set autocommit=0;
/*!40000 ALTER TABLE `finstat_year_portion` ENABLE KEYS */;
UNLOCK TABLES;
commit;

--
-- Dumping routines for database 'finstatdevel'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-02  9:24:38
