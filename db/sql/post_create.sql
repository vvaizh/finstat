﻿
ALTER TABLE finstat DROP FOREIGN KEY by_finstat_subject;
ALTER TABLE finstat DROP FOREIGN KEY by_finstat_year_portion;

alter table finstat_year_portion ENGINE = MyISAM;
alter table finstat_subject      ENGINE = MyISAM;
alter table finstat              ENGINE = MyISAM;

ALTER TABLE finstat ADD CONSTRAINT by_finstat_subject 
    FOREIGN KEY (id_finstat_subject)
    REFERENCES finstat_subject(id_finstat_subject)
;

ALTER TABLE finstat ADD CONSTRAINT by_finstat_year_portion 
    FOREIGN KEY (id_finstat_year_portion)
    REFERENCES finstat_year_portion(id_finstat_year_portion)
;

