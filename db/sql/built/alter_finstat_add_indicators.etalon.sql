/* ВНИМАНИЕ! */
/* данный файл генерируется автоматически */
/* при помощи модуля codec.Prepare_table.sql.js */
/* НЕ редактируйте этот файл вручную для избежания конфликтов! */


alter table finstat
   add f1110 bigint not null
 , add f1120 bigint not null
 , add f1130 bigint not null
 , add f1140 bigint not null
 , add f1150 bigint not null
 , add f1160 bigint not null
 , add f1170 bigint not null
 , add f1180 bigint not null
 , add f1190 bigint not null
 , add f1100 bigint not null
 , add f1210 bigint not null
 , add f1220 bigint not null
 , add f1230 bigint not null
 , add f1240 bigint not null
 , add f1250 bigint not null
 , add f1260 bigint not null
 , add f1200 bigint not null
 , add f1600 bigint not null
 , add f1310 bigint not null
 , add f1320 bigint not null
 , add f1340 bigint not null
 , add f1350 bigint not null
 , add f1360 bigint not null
 , add f1370 bigint not null
 , add f1300 bigint not null
 , add f1410 bigint not null
 , add f1420 bigint not null
 , add f1430 bigint not null
 , add f1450 bigint not null
 , add f1400 bigint not null
 , add f1510 bigint not null
 , add f1520 bigint not null
 , add f1530 bigint not null
 , add f1540 bigint not null
 , add f1550 bigint not null
 , add f1500 bigint not null
 , add f1700 bigint not null
 , add f2110 bigint not null
 , add f2120 bigint not null
 , add f2100 bigint not null
 , add f2210 bigint not null
 , add f2220 bigint not null
 , add f2200 bigint not null
 , add f2310 bigint not null
 , add f2320 bigint not null
 , add f2330 bigint not null
 , add f2340 bigint not null
 , add f2350 bigint not null
 , add f2300 bigint not null
 , add f2410 bigint not null
 , add f2421 bigint not null
 , add f2430 bigint not null
 , add f2450 bigint not null
 , add f2460 bigint not null
 , add f2400 bigint not null
 , add f2510 bigint not null
 , add f2520 bigint not null
 , add f2500 bigint not null
;
