
CREATE TABLE finstat(
    id_finstat                 INT           AUTO_INCREMENT,
    id_finstat_subject         INT,
    id_finstat_year_portion    INT           NOT NULL,
    okpo                       VARCHAR(8)    NOT NULL,
    okopf                      VARCHAR(5)    NOT NULL,
    okfs                       VARCHAR(2)    NOT NULL,
    okved                      VARCHAR(8)    NOT NULL,
    PRIMARY KEY (id_finstat)
)ENGINE=INNODB
;



CREATE TABLE finstat_subject(
    id_finstat_subject    INT            AUTO_INCREMENT,
    inn                   VARCHAR(10)    NOT NULL,
    name                  TEXT           NOT NULL,
    okpo                  VARCHAR(8)     NOT NULL,
    okopf                 VARCHAR(5)     NOT NULL,
    okfs                  VARCHAR(2)     NOT NULL,
    okved                 VARCHAR(8)     NOT NULL,
    PRIMARY KEY (id_finstat_subject)
)ENGINE=INNODB
;



CREATE TABLE finstat_year_portion(
    id_finstat_year_portion    INT             AUTO_INCREMENT,
    TimeLoaded                 TIMESTAMP       NOT NULL,
    FileName                   VARCHAR(255)    NOT NULL,
    year                       INT             NOT NULL,
    PRIMARY KEY (id_finstat_year_portion)
)ENGINE=INNODB
;



CREATE UNIQUE INDEX by_subject_year_portion ON finstat(id_finstat_subject, id_finstat_year_portion)
;
CREATE INDEX by_finstat_subject ON finstat(id_finstat_subject)
;
CREATE INDEX by_finstat_year_portion ON finstat(id_finstat_year_portion)
;
CREATE UNIQUE INDEX by_inn ON finstat_subject(inn)
;
CREATE UNIQUE INDEX by_year ON finstat_year_portion(year)
;
ALTER TABLE finstat ADD CONSTRAINT by_finstat_subject 
    FOREIGN KEY (id_finstat_subject)
    REFERENCES finstat_subject(id_finstat_subject)
;

ALTER TABLE finstat ADD CONSTRAINT by_finstat_year_portion 
    FOREIGN KEY (id_finstat_year_portion)
    REFERENCES finstat_year_portion(id_finstat_year_portion)
;


